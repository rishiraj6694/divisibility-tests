
# Divisibility Tests

This program gives divisibility tests for every digit in any base of your choice! Program used for [this blog post](https://risingentropy.com/divisibility-tricks-and-orderly-numbers/)

## Base 10 and base 16

![](Media/div-10-and-16.png)

## Base 39

![](Media/div-base-39.png)

## Plotting divisibility tests by base

![](Media/div-tests-plot.png)