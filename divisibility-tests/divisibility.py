
#checks coprimality of i and j
def coprime(i,j):
	n = 2
	while n <= min(i,j):
		if i%n == 0 and j%n == 0:
			return False
		n += 1
	return True

#checks if there's a sum test for k in base b
def sumTestFor(k,b):
	return (b-1)%k == 0

#checks if there's an alternating sum test for k in base b
def altSumTestFor(k,b):
	return (b+1)%k == 0

#checks if there's an ending test for k in base b
def endingTestFor(k,b):
	if b%k == 0:
		return 1
	else:
		for i in range(2,k):
			if b%i == 0:
				for j in range(2,k):
					if i**j == k:
						return j
	return None

#checks if there's a (X-na) test for k in base b
def specialTestFor(k,b):
	for n in range(k):
		if (b*n+1)%k == 0:
			return n
	return None

#checks if there's a sum, altsum, ending, or (X-na) test for k in base b
def simpleTestFor(k,b):
	if sumTestFor(k,b) or altSumTestFor(k,b):
		return True
	if endingTestFor(k,b) or specialTestFor(k,b):
		return True
	return False

#returns startset + all products of coprime pairs of elements of startset
def getSpecialProducts(startset, maximum, products):
	newset = set()
	for i in startset:
		newset.add(i)
		for j in startset:
			if coprime(i,j):
				if (i*j <= maximum):
					newset.add(i*j)
					products[i*j] = (i,j)
	return newset

#checks if there's a product test for k in base b
def productTestFor(k,b):
	start = set()
	products = {}
	for i in range(2,b):
		if simpleTestFor(i,b):
			start.add(i)
	maximum = max(b,k)
	new = getSpecialProducts(start,maximum,products)
	while new != getSpecialProducts(new,maximum,products):
		new = getSpecialProducts(new,maximum,products)
	if (k in new) and (k not in start):
		return products

#returns true if there's a divisibility test for k in base b
def testFor(k,b):
	return simpleTestFor(k,b) or productTestFor(k,b)

#narrates all the divisibility tests for all digits in base b
def narrateTests(b):
	for k in range(2,b):
		print(k, end=":")
		if sumTestFor(k,b):
			print("\tSum test")
		if altSumTestFor(k,b):
			print("\tAlternating sum test")
		if endingTestFor(k,b):
			n = endingTestFor(k,b)
			if n == 1:
				print("\tCheck last digit")
			else:
				print("\tCheck last", n, "digits")
		if specialTestFor(k,b):
			n = specialTestFor(k,b)
			if n == 1:
				print("\tXa is div by", k, "iff X-a is div by", k)
			else:
				print("\tXa is div by", k, "iff X-"+str(n)+"a is div by", k)
		if productTestFor(k,b):
			products = productTestFor(k,b)
			print("\tProduct test: Test", products[k][0], "and", products[k][1])
	print()

#prints out the divisibility tests in base b by categories
def describeCategories(b):
	print("\tSum Tests: ",end="\t")
	for k in range(2,b):
		if sumTestFor(k,b):
			print(k,end=" ")
	print("\n\tAlt Sum Tests: ",end="\t")
	for k in range(2,b):
		if altSumTestFor(k,b):
			print(k,end=" ")
	print("\n\tEnding Tests: ",end="\t")
	for k in range(2,b):
		if endingTestFor(k,b):
			print(k,end=" ")
	print("\n\t(X-na) Tests: ",end="\t")
	for k in range(2,b):
		if specialTestFor(k,b):
			print(k,end=" ")
	print("\n\tProduct Tests: ",end="\t")
	for k in range(2,b):
		if productTestFor(k,b):
			print(k,end=" ")
	print("\n")

#returns the number of ending tests in base b
def endingTests(b):
	count = 0
	for i in range(2,b):
		if endingTestFor(i,b):
			count += 1
	return count

#returns the number of sum tests in base b
def sumTests(b):
	count = 0
	for i in range(2,b):
		if sumTestFor(i,b):
			count += 1
	return count

#returns the number of alternating sum tests in base b
def altSumTests(b):
	count = 0
	for i in range(2,b):
		if altSumTestFor(i,b):
			count += 1
	return count

#returns the number of (x-na) tests in base b
def specialTests(b):
	count = 0
	for i in range(2,b):
		if specialTestFor(i,b):
			count += 1
	return count

#returns the total number of tests in base b
def totalTests(b):
	count = 0
	for i in range(2,b):
		if sumTestFor(i,b):
			count+=1
		if altSumTestFor(i,b):
			count+=1
		if endingTestFor(i,b):
			count+=1
		if specialTestFor(i,b):
			count+=1
		if productTestFor(i,b):
			count+=1
	return count

while True:
	b = int(input("Enter base: "))
	print()
	describeCategories(b)
	narrateTests(b)

